# АиСД. Работа №2: Верхнетреугольные матрицы на шаблонах
***


### Цели работы:   


##### В рамках лабораторной работы ставится задача создания программных средств, поддерживающих эффективное хранение матриц специального вида (верхнетреугольных) и выполнение основных операций над ними:  
- сложение/вычитание  
- копирование  
- сравнение  
***


### Задачи:  
- Реализация методов шаблонного класса `TVector` согласно заданному интерфейсу.  
- Реализация методов шаблонного класса `TMatrix` согласно заданному интерфейсу.  
- Обеспечение работоспособности тестов и примера использования.  
- Реализация заготовок тестов, покрывающих все методы классов `TVector` и `TMatrix`.  
- Модификация примера использования в тестовое приложение, позволяющее задавать матрицы и осуществлять основные операции над ними.
***


### Используемые инструменты:


- Система контроля версий Git. 
- Фреймворк для написания автоматических тестов Google Test.
- Среда разработки Microsoft Visual Studio (2015).
***


# Начало работы:
***

```c++
#ifndef __TMATRIX_H__
#define __TMATRIX_H__

#include <iostream>

using namespace std;

const int MAX_VECTOR_SIZE = 100000000;
const int MAX_MATRIX_SIZE = 10000;
```

Классы `TVector` и `TMatrix` (`TMatrix` наследует класс `TVector`) реализуются как шаблоны, поэтому объявление и реализация классов находятся в одном h-файле. (`utmatrix.h`)  
Константы заданные по умолчанию ограничивающие размеры создаваемых объектах:
Для контроля ошибок используется специальный оператор `throw`, при обнаружении ошибки она будет "выловлена", с поясняющим ошибку текстом.



***
### 1. Шаблон класса `TVector`:
***

#### Объявление:

```c++
template <class ValType>
class TVector
{
protected:
  ValType *pVector;
  int Size;       
  int StartIndex; 
public:
  TVector(int s = 10, int si = 0);
  TVector(const TVector &v);                
  ~TVector();
  int GetSize(){ return Size; } 
  int GetStartIndex(){ return StartIndex; } 
  ValType& operator[](int pos);             
  bool operator==(const TVector &v) const;  
  bool operator!=(const TVector &v) const;  
  TVector& operator=(const TVector &v);     

  TVector  operator+(const ValType &val);   
  TVector  operator-(const ValType &val);   
  TVector  operator*(const ValType &val);   

  TVector  operator+(const TVector &v);     
  TVector  operator-(const TVector &v);     
  ValType  operator*(const TVector &v);     

  friend istream& operator>>(istream &in, TVector &v)
  {
    for (int i = 0; i < v.Size; i++)
      in >> v.pVector[i];
    return in;
  }
  friend ostream& operator<<(ostream &out, const TVector &v)
  {
    for (int i = 0; i < v.Size; i++)
      out << v.pVector[i] << ' ';
    return out;
  }
};

```

#### Реализация:
Конструктор:
```c++
template <class ValType>
TVector<ValType>::TVector(int s, int si)
{
	if (s < 0 || si < 0 || s > MAX_VECTOR_SIZE || si > MAX_VECTOR_SIZE) throw "Ошибка: Неверные параметры вектора";
	Size = s;
	StartIndex = si;
	pVector = new ValType[Size];
	memset(pVector, 0, Size * sizeof(ValType));
} 
```
Конструктор копирования:
```c++
template <class ValType> 
TVector<ValType>::TVector(const TVector<ValType> &v)
{
	Size = v.Size;
	StartIndex = v.StartIndex;
	pVector = new ValType[Size];
	for (int i = 0; i < Size; i++)
		pVector[i] = v.pVector[i];
} 
```
Деструктор:
```c++
template <class ValType>
TVector<ValType>::~TVector()
{
	delete[] pVector;
} 
```
Доступ к элементу:
```c++
template <class ValType> 
ValType& TVector<ValType>::operator[](int pos)
{
	if (pos < StartIndex || pos >= StartIndex + Size) throw "Ошибка: Выход за границы вектора";
	return pVector[pos - StartIndex];
} 
```
Сравнение (проверка на равенство):
```c++
template <class ValType> 
bool TVector<ValType>::operator==(const TVector &v) const
{
	if (Size != v.Size || StartIndex != v.StartIndex) return false;
	for (int i = 0; i < Size; i++)
		if (pVector[i] != v.pVector[i])
		return false;
	return true;
	} 
```
Сравнение (проверка на неравенство):
```c++
template <class ValType> 
bool TVector<ValType>::operator!=(const TVector &v) const
{
	return !operator==(v);
} 
```
Конструктор присваивания:
```c++
template <class ValType> 
TVector<ValType>& TVector<ValType>::operator=(const TVector &v)
{
	if (v.pVector == NULL) throw "Вектор не существует";
	if (this == &v) 
	{
		return *this;
	}
	if (Size != v.Size)
		{
		delete[] pVector;
		pVector = new ValType[v.Size];
		Size = v.Size;
		}
	StartIndex = v.StartIndex;
	for (int i = 0; i < Size; i++)
		pVector[i] = v.pVector[i];
	return *this;
} 
```
Прибавить скаляр к вектору:
```c++
template <class ValType> 
TVector<ValType> TVector<ValType>::operator+(const ValType &val)
{
	TVector<ValType> temp(Size, StartIndex);
	for (int i = 0; i < temp.Size; i++)
		temp.pVector[i] = pVector[i] + val;
	return temp;
} 
```
Вычесть скаляр из вектора:
```c++
template <class ValType> 
TVector<ValType> TVector<ValType>::operator-(const ValType &val)
{
	return operator+(-val);
} 
```
Умножение на скаляр:
```c++
template <class ValType> 
TVector<ValType> TVector<ValType>::operator*(const ValType &val)
{
	TVector<ValType> temp(Size, StartIndex);
	for (int i = 0; i < temp.Size; i++)
		temp.pVector[i] = pVector[i] * val;
	return temp;
} 
```
Сумма векторов:
```c++
template <class ValType> 
TVector<ValType> TVector<ValType>::operator+(const TVector<ValType> &v)
{
	if (Size != v.Size || StartIndex != v.StartIndex) throw "Размеры векторов не совпадают";
	TVector<ValType> temp = TVector<ValType>(Size, StartIndex);
	for (int i = 0; i < Size; i++)
		temp.pVector[i] = pVector[i] + v.pVector[i];
	return temp;
} 
```
Разность векторов:
```c++
template <class ValType> 
TVector<ValType> TVector<ValType>::operator-(const TVector<ValType> &v)
{
	if (Size != v.Size || StartIndex != v.StartIndex) throw "Размеры векторов не совпадают";
	TVector<ValType> temp = TVector<ValType>(Size, StartIndex);
	for (int i = 0; i < Size; i++)
		temp.pVector[i] = pVector[i] - v.pVector[i];
	return temp;
} 
```
Скалярное произведение:
```c++
template <class ValType> 
ValType TVector<ValType>::operator*(const TVector<ValType> &v)
{
	if (Size != v.Size || StartIndex != v.StartIndex) throw "Размеры векторов не совпадают";
	ValType result = 0;
	for (int i = 0; i < Size; i++)
		result += pVector[i] * v.pVector[i];
	return result;
} 
```

***
### 2. Шаблон класса `TMatrix`:
***

#### Объявление:

```c++
template <class ValType>
class TMatrix : public TVector<TVector<ValType> >
{
	public:
		TMatrix(int s = 10);
		TMatrix(const TMatrix &mt);                    
		TMatrix(const TVector<TVector<ValType> > &mt); 
		bool operator==(const TMatrix &mt) const;      
		bool operator!=(const TMatrix &mt) const;      
		TMatrix& operator= (const TMatrix &mt);        
		TMatrix  operator+ (const TMatrix &mt);        
		TMatrix  operator- (const TMatrix &mt);        
		friend istream& operator >> (istream &in, TMatrix &mt)
		{
			for (int i = 0; i < mt.Size; i++)
				in >> mt.pVector[i];
			return in;
		}
		friend ostream & operator<<(ostream &out, const TMatrix &mt)
		{
		    for (int i = 0; i < mt.Size; i++)
				out << mt.pVector[i] << endl;
			return out;
		}
};

```

#### Реализация:
Конструктор:
```c++
template <class ValType>
TMatrix<ValType>::TMatrix(int s) : TVector<TVector<ValType> >(s)
{
	if (s <= 0 || s > MAX_MATRIX_SIZE) throw "Некорректный размер матрицы!";
	for (int i = 0; i<s; i++)
		pVector[i] = TVector<ValType>(s - i, i);
} 
```
Конструктор копирования:
```c++
template <class ValType> 
TMatrix<ValType>::TMatrix(const TMatrix<ValType> &mt) : TVector<TVector<ValType> >(mt) {}
```
Конструктор преобразования типа:
```c++
template <class ValType> 
TMatrix<ValType>::TMatrix(const TVector<TVector<ValType> > &mt) : TVector<TVector<ValType> >(mt) {}
```
Сравнение (проверка на равенство):
```c++
template <class ValType> 
bool TMatrix<ValType>::operator==(const TMatrix<ValType> &mt) const
{
	return TVector<TVector<ValType> >::operator==(mt);
} 
```
Сравнение (проверка на неравенство):
```c++
template <class ValType> 
bool TMatrix<ValType>::operator!=(const TMatrix<ValType> &mt) const
{
	return TVector<TVector<ValType> >::operator!=(mt);
} 
```
Конструктор присваивания:
```c++
template <class ValType> 
TMatrix<ValType>& TMatrix<ValType>::operator=(const TMatrix<ValType> &mt)
{
	TVector<TVector<ValType> >::operator=(mt);
	return *this;
} 
```
Сумма матриц:
```c++
template <class ValType> 
TMatrix<ValType> TMatrix<ValType>::operator+(const TMatrix<ValType> &mt)
{
	return TVector<TVector<ValType> >::operator+(mt);
} 
```
Разность матриц
```c++
template <class ValType> 
TMatrix<ValType> TMatrix<ValType>::operator-(const TMatrix<ValType> &mt)
{
		return TVector<TVector<ValType> >::operator-(mt);
} 
#endif
```
***
### 3. Реализация тестов:
***

#### 3.А Тесты для класса `TVector`:

```c++
#include "utmatrix.h"

#include <gtest.h>

TEST(TVector, can_create_vector_with_positive_length)
{
  ASSERT_NO_THROW(TVector<int> v(5));
}
```
```c++
TEST(TVector, cant_create_too_large_vector)
{
  ASSERT_ANY_THROW(TVector<int> v(MAX_VECTOR_SIZE + 1));
}
```
```c++
TEST(TVector, throws_when_create_vector_with_negative_length)
{
  ASSERT_ANY_THROW(TVector<int> v(-5));
}
```
```c++
TEST(TVector, throws_when_create_vector_with_negative_startindex)
{
  ASSERT_ANY_THROW(TVector<int> v(5, -2));
}
```
```c++
TEST(TVector, can_create_copied_vector)
{
  TVector<int> v(10);

  ASSERT_NO_THROW(TVector<int> v1(v));
}
```
```c++
TEST(TVector, copied_vector_is_equal_to_source_one)
{
	const int size = 5;
	TVector<int> v1(size);
	for (int i = 0; i < size; i++)
		v1[i] = 0;
	TVector<int> v2(v1);
	EXPECT_EQ(v1, v2);
}
```
```c++
TEST(TVector, copied_vector_has_its_own_memory)
{
	const int size = 5;
	TVector<int> v1(size);
	for (int i = 0; i < size; i++)
		v1[i] = 0;
	TVector<int> v2(v1);
	v1[0] = 1;
	EXPECT_NE(v1, v2);
}
```
```c++
TEST(TVector, can_get_size)
{
	TVector<int> v(4);
	
	EXPECT_EQ(4, v.GetSize());
}
```
```c++
TEST(TVector, can_get_start_index)
{
	TVector<int> v(4, 2);

	EXPECT_EQ(2, v.GetStartIndex());
}
```
```c++
TEST(TVector, can_set_and_get_element)
{
	TVector<int> v(4);
	v[0] = 4;

	EXPECT_EQ(4, v[0]);
}
```
```c++
TEST(TVector, throws_when_set_element_with_negative_index)
{
	TVector<int> v(5);
	
	ASSERT_ANY_THROW(v[-1] = 0);
}
```
```c++
TEST(TVector, throws_when_set_element_with_too_large_index)
{
	TVector<int> v(5);
	
	ASSERT_ANY_THROW(v[7] = 0);
}
```
```c++
TEST(TVector, can_assign_vector_to_itself)
{
	const int size = 4;
	TVector<int> v(size);
	for (int i = 0; i < size; i++)
		v[i] = 0;
	ASSERT_NO_THROW(v = v);
}
```
```c++
TEST(TVector, can_assign_vectors_of_equal_size)
{
	const int size = 5;
	TVector<int> v1(size), v2(size);
	for (int i = 0; i < size; i++)
		v2[i] = 0;
	ASSERT_NO_THROW(v1 = v2);
}
```
```c++
TEST(TVector, assign_operator_change_vector_size)
{
	const int size_1 = 5, size_2 = 7;
	TVector<int> v1(size_1), v2(size_2);
	for (int i = 0; i < size_2; i++)
		v2[i] = 0;
	v1 = v2;
	EXPECT_NE(size_1, v1.GetSize());
}
```
```c++
TEST(TVector, can_assign_vectors_of_different_size)
{
	const int size_1 = 5, size_2 = 7;
	TVector<int> v1(size_1), v2(size_2);
	for (int i = 0; i < size_2; i++)
		v2[i] = 0;
	ASSERT_NO_THROW(v1 = v2);
}
```
```c++
TEST(TVector, compare_equal_vectors_return_true)
{
	const int size = 5;
	TVector<int> v1(size);
	for (int i = 0; i < size; i++)
		v1[i] = 0;
	TVector<int> v2(v1);
	EXPECT_TRUE(v1 == v2);
}
```
```c++
TEST(TVector, compare_vector_with_itself_return_true)
{
	const int size = 5;
	TVector<int> v(size);
	for (int i = 0; i < size; i++)
		v[i] = 0;
	EXPECT_TRUE(v == v);
}
```
```c++
TEST(TVector, vectors_with_different_size_are_not_equal)
{
	const int size_1 = 5, size_2 = 7;
	TVector<int> v1(size_1), v2(size_2);
	for (int i = 0; i < size_2; i++)
	{
		if (i < size_1)
			v1[i] = 0;
		v2[i] = 0;
	}
	EXPECT_TRUE(v1 != v2);
}
```
```c++
TEST(TVector, can_add_scalar_to_vector)
{
	const int size = 5;
	TVector<int> v1(size), v2(size);
	for (int i = 0; i < size; i++)
	{	
		v1[i] = 1;
		v2[i] = 6;
	}
	EXPECT_EQ(v1 + 5, v2);
}
```
```c++
TEST(TVector, can_subtract_scalar_from_vector)
{
	const int size = 5;
	TVector<int> v1(size), v2(size);
	for (int i = 0; i < size; i++)
	{
		v1[i] = 7;
		v2[i] = 2;
	}
	EXPECT_EQ(v1 - 5, v2);
}
```
```c++
TEST(TVector, can_multiply_scalar_by_vector)
{
	const int size = 4;
	TVector<int> v1(size), v2(size);
	for (int i = 0; i < size; i++)
	{
		v1[i] = 3;
		v2[i] = 9;
	}
	EXPECT_EQ(v1 * 3, v2);
}
```
```c++
TEST(TVector, can_add_vectors_with_equal_size)
{
	const int size = 5;
	TVector<int> v1(size), v2(size), v3(size);
	for (int i = 0; i < size; i++)
	{
		v1[i] = 3;
		v2[i] = 5;
		v3[i] = 8;
	}
	EXPECT_EQ(v1 + v2, v3);
}
```
```c++
TEST(TVector, cant_add_vectors_with_not_equal_size)
{
	const int size_1 = 5, size_2 = 7;
	TVector<int> v1(size_1), v2(size_2);
	for (int i = 0; i < size_2; i++)
	{
		if (i < size_1)
			v1[i] = 1;
		v2[i] = 1;
	}
	ASSERT_ANY_THROW(v1 + v2);
}
```
```c++
TEST(TVector, can_subtract_vectors_with_equal_size)
{
	const int size = 5;
	TVector<int> v1(size), v2(size), v3(size);
	for (int i = 0; i < size; i++)
	{
		v1[i] = 8;
		v2[i] = 5;
		v3[i] = 3;
	}
	EXPECT_EQ(v1 - v2, v3);
}
```
```c++
TEST(TVector, cant_subtract_vectors_with_not_equal_size)
{
	const int size_1 = 5, size_2 = 7;
	TVector<int> v1(size_1), v2(size_2);
	for (int i = 0; i < size_2; i++)
	{
		if (i < size_1)
			v1[i] = 1;
		v2[i] = 1;
	}
	ASSERT_ANY_THROW(v1 - v2);
}
```
```c++
TEST(TVector, can_multiply_vectors_with_equal_size)
{
	const int size = 5;
	TVector<int> v1(size), v2(size);
	for (int i = 0; i < size; i++)
		v1[i] = v2[i] = 3;
	EXPECT_EQ(45, v1 * v2);
}
```
```c++
TEST(TVector, cant_multiply_vectors_with_not_equal_size)
{
	const int size_1 = 4, size_2 = 6;
	TVector<int> v1(size_1), v2(size_2);
	for (int i = 0; i < size_2; i++)
	{
		if (i < size_1)
			v1[i] = 1;
		v2[i] = 1;
	}
	ASSERT_ANY_THROW(v1 * v2);
}
```

#### 3.Б Тесты для класса `TMatrix`:

```c++
#include "utmatrix.h"

#include <gtest.h>

TEST(TMatrix, can_create_matrix_with_positive_length)
{
  ASSERT_NO_THROW(TMatrix<int> m(5));
}
```
```c++
TEST(TMatrix, cant_create_too_large_matrix)
{
  ASSERT_ANY_THROW(TMatrix<int> m(MAX_MATRIX_SIZE + 1));
}
```
```c++
TEST(TMatrix, throws_when_create_matrix_with_negative_length)
{
  ASSERT_ANY_THROW(TMatrix<int> m(-5));
}
```
```c++
TEST(TMatrix, can_create_copied_matrix)
{
  TMatrix<int> m(5);

  ASSERT_NO_THROW(TMatrix<int> m1(m));
}
```
```c++
TEST(TMatrix, copied_matrix_is_equal_to_source_one)
{
	const int size = 3;
	TMatrix<int> m1(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
		m1[i][j] = 0;
	TMatrix<int> m2(m1);
	EXPECT_EQ(m1, m2);
}
```
```c++
TEST(TMatrix, copied_matrix_has_its_own_memory)
{
	const int size = 3;
	TMatrix<int> m1(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
		m1[i][j] = 0;
	TMatrix<int> m2(m1);
	m1[0][0] = 1;
	EXPECT_NE(m1, m2);
}
```
```c++
TEST(TMatrix, can_get_size)
{
	TMatrix<int> m(5);
	EXPECT_EQ(5, m.GetSize());
}
```
```c++
TEST(TMatrix, can_set_and_get_element)
{
	TMatrix<int> m(5);
	m[0][0] = 5;
	EXPECT_EQ(5, m[0][0]);
}
```
```c++
TEST(TMatrix, throws_when_set_element_with_negative_index)
{
	TMatrix<int> m(5);
	ASSERT_ANY_THROW(m[-1] = 0);
}
```
```c++
TEST(TMatrix, throws_when_set_element_with_too_large_index)
{
	TMatrix<int> m(5);
	ASSERT_ANY_THROW(m[10] = 0);
}
```
```c++
TEST(TMatrix, can_assign_matrix_to_itself)
{
	const int size = 5;
	TMatrix<int> m(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
			m[i][j] = 0;
	ASSERT_NO_THROW(m = m);
}
```
```c++
TEST(TMatrix, can_assign_matrices_of_equal_size)
{
	const int size = 5;
	TMatrix<int> m1(size), m2(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
			m2[i][j] = 0;
	ASSERT_NO_THROW(m1 = m2);
}
```
```c++
TEST(TMatrix, assign_operator_change_matrix_size)
{
	const int size_1 = 4, size_2 = 7;
	TMatrix<int> m1(size_1), m2(size_2);
	for (int i = 0; i < size_2; i++)
	     for (int j = i; j < size_2; j++)
			 m2[i][j] = 0;
	m1 = m2;
	EXPECT_NE(size_1, m1.GetSize());
}
```
```c++
TEST(TMatrix, can_assign_matrices_of_different_size)
{
	const int size_1 = 4, size_2 = 7;
	TMatrix<int> m1(size_1), m2(size_2);
	for (int i = 0; i < size_2; i++)
		for (int j = i; j < size_2; j++)
			m2[i][j] = 0;
	ASSERT_NO_THROW(m1 = m2);
}
```
```c++
TEST(TMatrix, compare_equal_matrices_return_true)
{
	const int size = 5;
	TMatrix<int> m1(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
			m1[i][j] = 0;
	TMatrix<int> m2(m1);
	EXPECT_TRUE(m1 == m2);
}
```
```c++
TEST(TMatrix, compare_matrix_with_itself_return_true)
{
	const int size = 5;
	TMatrix<int> m(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
			m[i][j] = 0;
	EXPECT_TRUE(m == m);
}
```
```c++
TEST(TMatrix, matrices_with_different_size_are_not_equal)
{
	const int size_1 = 5, size_2 = 7;
	TMatrix<int> m1(size_1), m2(size_2);
	EXPECT_TRUE(m1 != m2);
}
```
```c++
TEST(TMatrix, can_add_matrices_with_equal_size)
{
	const int size = 5;
	TMatrix<int> m1(size), m2(size), m3(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
		{
			m1[i][j] = 3;
			m2[i][j] = 5;
			m3[i][j] = 8;
		}
	EXPECT_EQ(m1 + m2, m3);
}
```
```c++
TEST(TMatrix, cant_add_matrices_with_not_equal_size)
{
	const int size_1 = 4, size_2 = 6;
	TMatrix<int> m1(size_1), m2(size_2);
	ASSERT_ANY_THROW(m1 + m2);
}
```
```c++
TEST(TMatrix, can_subtract_matrices_with_equal_size)
{
	const int size = 5;
	TMatrix<int> m1(size), m2(size), m3(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
		{
			m1[i][j] = 5;
			m2[i][j] = 3;
			m3[i][j] = 2;
		}
	EXPECT_EQ(m1 - m2, m3);
}
```
```c++
TEST(TMatrix, cant_subtract_matrixes_with_not_equal_size)
{
	const int size_1 = 3, size_2 = 6;
	TMatrix<int> m1(size_1), m2(size_2);	
	ASSERT_ANY_THROW(m1 - m2);
}
```
***
### 4. Обеспечение работоспособности тестов:
***

#### 4.А Прохождение тестов для класса `TVector`:

![](http://s019.radikal.ru/i638/1611/8e/6c10c823ef54.png)

#### 4.Б Прохождение тестов для класса `TMatrix`:

![](http://i045.radikal.ru/1611/ca/e1447a2e701c.png)

#### Общий результат:

![](http://s017.radikal.ru/i416/1611/d6/97d7c387a4de.png)

***
### 5. Модификация примера использования:
***

#### Код тестового приложения:

```c++
#include <iostream>
#include "utmatrix.h"

void main()
{
	try
	{
		int size;

		setlocale(LC_ALL, "Russian");
		cout << "Тестирование программ поддержки представления треугольных матриц"<< endl;
		cout << "Введите размер треугольных матриц (одно число т.к. матрица симметричная)" << endl;
		cin >> size;
		TMatrix<int> a(size), b(size), c(size);
		
		cout << "Введите матрицу (элементы матрицы) a:" << endl;
		cin >> a;

		cout << "Введите матрицу (элементы матрицы) b:" << endl;
		cin >> b;

		cout << "Matrix a=" << endl << a << endl;
		cout << "Matrix b=" << endl << b << endl;
		c = a + b;
		cout << "Matrix c=a+b" << endl << c << endl;
		c = a - b;
		cout << "Matrix c=a-b" << endl << c << endl;
		c = a;
		cout << "Matrix c = Matrix a" << endl << c << endl;
		cout << "Matrix c == Matrix a?" << endl << (c==a) << endl;
		cout << "Matrix c != Matrix b?" << endl << (c!=b) << endl;
	}
	catch (int err)
	{
		cout << "Error: " << err << endl;
	}
}

```
***
#### Результат работы тестового приложения:
***
![](http://s019.radikal.ru/i639/1611/80/b746f3c3bc6b.png
)
***
# Вывод:
***
##### Все цели и задачи работы достигнуты. В ходе выполнения данной работы, мне удалось реализовать два класса:
-  `TVector` для работы с векторами. 
-  `TMatrix` для работы с верхнетреугольными матрицами. 
##### Также  мной были написаны большая часть тестов под систему тестирования `Google Test`, что позволяет не только проверить правильность работы программы, но и также лучше ознакомиться с `Google Test Framework`. Успешное же прохождение всех тестов показывает, что классы были реализованы корректно и программа созданная с использованием этих классов работает верно. 
##### Особенностью данной работы было то, что:
1. Класс `TMatrix` наследует класс `TVector`. 
2. Реализация классов `TMatrix` и `TVector` основана на использовании шаблонов, что позволяет сделать данные классы универсальными.
3. Большая часть тестов была реализовано самостоятельно.
4. Такжке используя наследуемость классов и шаблоны, программа стала намного меньше, чем при аналогичном задании без них.
