#include <iostream>
#include "utmatrix.h"

void main()
{
	try
	{
		int size;

		setlocale(LC_ALL, "Russian");
		cout << "������������ �������� ��������� ������������� ����������� ������"<< endl;
		cout << "������� ������ ����������� ������ (���� ����� �.�. ������� ������������)" << endl;
		cin >> size;
		TMatrix<int> a(size), b(size), c(size);
		
		cout << "������� ������� (�������� �������) a:" << endl;
		cin >> a;

		cout << "������� ������� (�������� �������) b:" << endl;
		cin >> b;

		cout << "Matrix a=" << endl << a << endl;
		cout << "Matrix b=" << endl << b << endl;
		c = a + b;
		cout << "Matrix c=a+b" << endl << c << endl;
		c = a - b;
		cout << "Matrix c=a-b" << endl << c << endl;
		c = a;
		cout << "Matrix c = Matrix a" << endl << c << endl;
		cout << "Matrix c == Matrix a?" << endl << (c==a) << endl;
		cout << "Matrix c != Matrix b?" << endl << (c!=b) << endl;
	}
	catch (int err)
	{
		cout << "Error: " << err << endl;
	}
}