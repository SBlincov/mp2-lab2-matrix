// ������������ ����������������� �������

#include <iostream>
#include "utmatrix.h"
//---------------------------------------------------------------------------

int main()
{
	setlocale(LC_ALL, "Russian");
	unsigned int n;
	cout << "������������ ������ TMatrix �� ������� ������������ ������� � ������������� ������������" << '\n' << endl;
	cout << "������� ������� n >>" << '\n' << endl;
	cin >> n;
	cout << '\n';
	TMatrix<int> triangle(n + 1);
	int i, j, sum = 0, t_Size = triangle.GetSize();
	triangle[t_Size - 1][t_Size - 1] = 1;
	for (i = t_Size - 2; i >= 0; i--)
	{
		for (j = i; j < t_Size; j++)
		if ((j == i) || (j == t_Size - 1))
			triangle[i][j] = 1;
		else
			triangle[i][j] = triangle[i + 1][j + 1] + triangle[i + 1][j];
	}
	for (i = 0; i < t_Size; i++)
		sum += triangle[0][i];
	cout << "������������� ����������� ��� " << n << " �������: " << '\n' << triangle[0] << '\n' << endl;
	cout << "����������� �������: \n" << triangle << '\n' << endl;
	cout << "����� ������������ ������ � ������� " << n << " : " << sum << " = 2^" << n << endl;
	return 0;
}
//---------------------------------------------------------------------------