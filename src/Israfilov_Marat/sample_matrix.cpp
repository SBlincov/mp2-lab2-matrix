// ����, ���, ���� "������ ����������������-2", �++, ���
//
// sample_matrix.cpp - Copyright (c) ������� �.�. 07.05.2001
//   ������������ ��� Microsoft Visual Studio 2008 �������� �.�. (20.04.2015)
//
// ������������ ����������������� �������

#include <iostream>
#include "utmatrix.h"
//---------------------------------------------------------------------------

void main()
{
	try
	{		
		int i, j, size;		

		setlocale(LC_ALL, "Russian");
		cout << "������������ �������� ��������� ������������� ����������� ������ \n������� ������ ������: ";
		cin >> size;
		TMatrix<int> a(size), b(size), c(size);

		cout << "������� ������� a:" << endl;
		cin >> a;

		cout << "������� ������� b:" << endl;
		cin >> b;
  
		cout << "Matrix a = " << endl << a << endl;
		cout << "Matrix b = " << endl << b << endl;

		c = a + b;
		cout << "Matrix c = a + b" << endl << c << endl;

		c = a - b;
		cout << "Matrix c = a - b" << endl << c << endl;

		c = b;
		cout << "Matrix c = b" << endl << c << endl;
		cout << "c == b" << endl << (c == b) << endl;
		cout << "c == a" << endl << (c == a) << endl;

	}
	catch (int err)
	{
		cout << "Error �" << err << endl;
	}
}
//---------------------------------------------------------------------------
