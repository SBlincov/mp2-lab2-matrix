# ������ ���������������� 2: ����������������� ������� �� ��������

## ��������

� ������ ������������ ������ �������� ������ �������� ����������� �������, �������������� ����������� �������� ������ ������������ ���� (�����������������) � ���������� �������� �������� ��� ����:

- ��������/���������;
- �����������;
- ���������.

����� ����������� ������ ��� ������� ������-������, ���������� ���������:

 - ���������� ������� ������ � ������� (h-����)
 - ��������� ����� ������� ������ ��� ������� �� ��������� �������.
 - ����� ��������� ������ ��� ������� �� ��������� �������. 
 - �������� ������ ������������� ������ �������

���������� ������ ������������ ������� ��������� �����:

  1. ���������� ������� ���������� ������ `TVector` �������� ��������� ����������.
  1. ���������� ������� ���������� ������ `TMatrix` �������� ��������� ����������.
  1. ����������� ����������������� ������ � ������� �������������.
  1. ���������� ��������� ������, ����������� ��� ������ ������� `TVector` � `TMatrix`.
  1. ����������� ������� ������������� � �������� ����������.

## �������� ������� TVector � TMatrix

����������������� ������� ������������ � ���� �������. ��� ������ � �������� ���� ����������� ������ ������ TVector. �������- ��� ������ ��� �������� ����� ��� ������ ������� � ����� ������ TVector. ���������� ������� ������ TMatrix ������������� ���������� ������� ������ TVector.

##### ���� ������, ������� ����� ���������� �� ����� ������ � ����������:

��� ������: 1. Incorrect value of size or StartIndex
��� ������: 2. Incorrect index of Vector
��� ������: 3. Can't sum matrices. Size of first matrix not even size of second matrix
��� ������: 4. Can't sum vectors. Size of first vect must even size of second vect 
��� ������: 5. Size of matrix is incorrect. Can't calculate scalar mult
��� ������: 6. Size of matrix is set incorrectly

## ���������� ����� utmatrix.h (���������� ������� TVector � TMatrix):
    
    // ����, ���, ���� "������ ����������������-2", �++, ���
    //
    // utmatrix.h - Copyright (c) ������� �.�. 07.05.2001
    //   ������������ ��� Microsoft Visual Studio 2008 �������� �.�. (21.04.2015)
    //
    // ����������������� ������� - ���������� �� ������ ������� �������
    
    #ifndef __TMATRIX_H__
    #define __TMATRIX_H__
    
    #include <iostream>
    
    using namespace std;
    
    const int MAX_VECTOR_SIZE = 100000000;
    const int MAX_MATRIX_SIZE = 10000;
    
    // ������ �������
    template <class ValType>
    class TVector
    {
    protected:
      ValType *pVector;
      int Size;       // ������ �������
      int StartIndex; // ������ ������� �������� �������
    public:
      TVector(int s = 10, int si = 0);
      TVector(const TVector &v);                // ����������� �����������
      ~TVector();
      int GetSize()      { return Size;       } // ������ �������
      int GetStartIndex(){ return StartIndex; } // ������ ������� ��������
      ValType& operator[](int pos);             // ������
      bool operator==(const TVector &v) const;  // ���������
      bool operator!=(const TVector &v) const;  // ���������
      TVector& operator=(const TVector &v);     // ������������
    
      // ��������� ��������
      TVector  operator+(const ValType &val);   // ��������� ������
      TVector  operator-(const ValType &val);   // ������� ������
      TVector  operator*(const ValType &val);   // �������� �� ������
    
      // ��������� ��������
      TVector  operator+(const TVector &v);     // ��������
      TVector  operator-(const TVector &v);     // ���������
      ValType  operator*(const TVector &v);     // ��������� ������������
    
      // ����-�����
      friend istream& operator>>(istream &in, TVector &v)
      {
        for (int i = 0; i < v.Size; i++)
          in >> v.pVector[i];
        return in;
      }
      friend ostream& operator<<(ostream &out, const TVector &v)
      {
        for (int i = 0; i < v.Size; i++)
          out << v.pVector[i] << ' ';
        return out;
      }
    };
    
    template <class ValType>
    TVector<ValType>::TVector(int s, int si){
    	if (s >= 0 && si >= 0 && s <= MAX_VECTOR_SIZE) {
    		Size = s;
    		StartIndex = si;
    		pVector = new ValType[Size];
    	}
    	else{
    		throw 1;
    	}
    } /*-------------------------------------------------------------------------*/
    
    template <class ValType> //����������� �����������
    TVector<ValType>::TVector(const TVector<ValType> &v){
    	Size = v.Size;
    	StartIndex = v.StartIndex;
    	pVector = new ValType[Size];
    
    	for (int i = StartIndex; i < Size; i++)
    		pVector[i] = v.pVector[i];
    } /*-------------------------------------------------------------------------*/
    
    template <class ValType>
    TVector<ValType>::~TVector(){
    	delete[] pVector;
    } /*-------------------------------------------------------------------------*/
    
    template <class ValType> // ������
    ValType& TVector<ValType>::operator[](int pos){
    	if (pos >= StartIndex && pos <= Size + StartIndex - 1)
    		return pVector[pos - StartIndex];
    	else
    		throw 2;
    } /*-------------------------------------------------------------------------*/
    
    template <class ValType> // ���������
    bool TVector<ValType>::operator==(const TVector &v) const{
    	if (Size == v.Size) {
    		for (int i = 0; i < Size; i++) {
    			if (pVector[i] != v.pVector[i])
    				return 0;
    		}
    	}
    	else
    		return 0;
    
    	return 1;
    } /*-------------------------------------------------------------------------*/
    
    template <class ValType> // ���������
    bool TVector<ValType>::operator!=(const TVector &v) const{
    	return !(*this == v);
    } /*-------------------------------------------------------------------------*/
    
    template <class ValType> // ������������
    TVector<ValType>& TVector<ValType>::operator=(const TVector &v){
    	if (this == &v) {
    		return *this;
    	}
    	else {
    		Size = v.Size;
    		StartIndex = v.StartIndex;
    		delete[] pVector;
    		pVector = new ValType[Size];
    
    		for (int i = 0; i < Size; i++) {
    			pVector[i] = v.pVector[i];
    		}
    
    		return *this;
    	}
    } /*-------------------------------------------------------------------------*/
    
    template <class ValType> // ��������� ������
    TVector<ValType> TVector<ValType>::operator+(const ValType &val){
    	TVector<ValType> ans(*this);
    
    	for (int i = 0; i < Size; i++) {
    		ans.pVector[i] = ans.pVector[i] + val;
    	}
    
    	return ans;
    } /*-------------------------------------------------------------------------*/
    
    template <class ValType> // ������� ������
    TVector<ValType> TVector<ValType>::operator-(const ValType &val){
    	ValType ans = *this + val * (-1); //Check me
    	return ans;
    } /*-------------------------------------------------------------------------*/
    
    template <class ValType> // �������� �� ������
    TVector<ValType> TVector<ValType>::operator*(const ValType &val){
    	TVector<ValType> ans(*this);
    
    	for (int i = 0; i < Size; i++) {
    		ans.pVector[i] = ans.pVector[i] * val;
    	}
    
    	return ans;
    } /*-------------------------------------------------------------------------*/
    
    template <class ValType> // ��������
    TVector<ValType> TVector<ValType>::operator+(const TVector<ValType> &v){
    	TVector<ValType> ans(*this);
    
    	if (Size == v.Size)
    		for (int i = 0; i < Size; i++)
    			ans.pVector[i] = pVector[i] + v.pVector[i];
    	else
    		throw 3;
    
    	return ans;
    } /*-------------------------------------------------------------------------*/
    
    template <class ValType> // ���������
    TVector<ValType> TVector<ValType>::operator-(const TVector<ValType> &v){
    	TVector<ValType> ans(*this);
    
    	if (Size == v.Size)
    		for (int i = 0; i < Size; i++)
    			ans.pVector[i] = pVector[i] - v.pVector[i];
    	else
    		throw 4;
    
    	return ans;
    } /*-------------------------------------------------------------------------*/
    
    template <class ValType> // ��������� ������������
    ValType TVector<ValType>::operator*(const TVector<ValType> &v){
    	ValType ans = 0;
    
    	if (Size == v.Size) {
    		for (int i = 0; i < Size; i++) {
    			ans = ans +  //!!
    				pVector[i] * v.pVector[i];
    		}
    	}
    	else {
    		throw 5;
    	}
    	return ans;
    } /*-------------------------------------------------------------------------*/
    
    
    // ����������������� �������
    template <class ValType>
    class TMatrix : public TVector<TVector<ValType> >
    {
    public:
      TMatrix(int s = 10);                           
      TMatrix(const TMatrix &mt);                    // �����������
      TMatrix(const TVector<TVector<ValType> > &mt); // �������������� ����
      bool operator==(const TMatrix &mt) const;      // ���������
      bool operator!=(const TMatrix &mt) const;      // ���������
      TMatrix& operator= (const TMatrix &mt);        // ������������
      TMatrix  operator+ (const TMatrix &mt);        // ��������
      TMatrix  operator- (const TMatrix &mt);        // ���������
    
      // ���� / �����
      friend istream& operator>>(istream &in, TMatrix &mt)
      {
        for (int i = 0; i < mt.Size; i++)
          in >> mt.pVector[i];
        return in;
      }
      friend ostream & operator<<( ostream &out, const TMatrix &mt)
      {
        for (int i = 0; i < mt.Size; i++)
          out << mt.pVector[i] << endl;
        return out;
      }
    };
    
    template <class ValType>
    TMatrix<ValType>::TMatrix(int s) : TVector<TVector<ValType> >(s){
    	if (s >= 0 && s <= MAX_MATRIX_SIZE) {
    	for (int i = 0; i < s; i++)
    		pVector[i] = TVector<ValType>(s - i, i);
    	}
    	else
    		throw 6;
    } /*-------------------------------------------------------------------------*/
    
    template <class ValType> // ����������� �����������
    TMatrix<ValType>::TMatrix(const TMatrix<ValType> &mt) :
    	TVector<TVector<ValType> >(mt) {}
    
    template <class ValType> // ����������� �������������� ����
    TMatrix<ValType>::TMatrix(const TVector<TVector<ValType> > &mt) :
    	TVector<TVector<ValType> >(mt) {}
    
    template <class ValType> // ���������
    bool TMatrix<ValType>::operator==(const TMatrix<ValType> &mt) const{
    	return TVector<TVector<ValType> >::operator==(mt);
    } /*-------------------------------------------------------------------------*/
    
    template <class ValType> // ���������
    bool TMatrix<ValType>::operator!=(const TMatrix<ValType> &mt) const{
    	return TVector<TVector<ValType> >::operator!=(mt);
    } /*-------------------------------------------------------------------------*/
    
    template <class ValType> // ������������
    TMatrix<ValType>& TMatrix<ValType>::operator=(const TMatrix<ValType> &mt){
    	if (this == &mt) {
    		return *this; //Matrices are equal
    	}
    	else {
    		Size = mt.Size;
    		StartIndex = mt.StartIndex;
    		delete[] pVector;
    
    		pVector = new TVector<ValType>[Size];
    		for (int i = 0; i < Size; i++)
    			pVector[i] = mt.pVector[i];
    
    		return *this;
    	}
    } /*-------------------------------------------------------------------------*/
    
    template <class ValType> // ��������
    TMatrix<ValType> TMatrix<ValType>::operator+(const TMatrix<ValType> &mt){
    	return TVector<TVector<ValType> >::operator+(mt);
    } /*-------------------------------------------------------------------------*/
    
    template <class ValType> // ���������
    TMatrix<ValType> TMatrix<ValType>::operator-(const TMatrix<ValType> &mt){
    	return TVector<TVector<ValType> >::operator-(mt);
    } /*-------------------------------------------------------------------------*/
    
    // TVector �3 �2 �4 �6
    // TMatrix �2 �2 �3 �3
    #endif


## ������������ ���������

### ����� ��� ������ TVector

    #include "utmatrix.h"
    
    #include <gtest.h>
    
    TEST(TVector, can_create_vector_with_positive_length) {
    	ASSERT_NO_THROW(TVector<int> v(5));
    }
    
    TEST(TVector, cant_create_too_large_vector) {
    	ASSERT_ANY_THROW(TVector<int> v(MAX_VECTOR_SIZE + 1));
    }
    
    TEST(TVector, throws_when_create_vector_with_negative_length) {
    	ASSERT_ANY_THROW(TVector<int> v(-5));
    }
    
    TEST(TVector, throws_when_create_vector_with_negative_startindex) {
    	ASSERT_ANY_THROW(TVector<int> v(5, -2));
    }
    
    TEST(TVector, can_create_copied_vector) {
    	TVector<int> v1(10);
    
    	ASSERT_NO_THROW(TVector<int> v2(v1));
    }
    
    TEST(TVector, copied_vector_is_equal_to_source_one) {
    	TVector<int> v1(10), v2(v1);
    
    	EXPECT_EQ(v1, v2);
    }
    
    TEST(TVector, copied_vector_has_its_own_memory) {
    	TVector<int> v1(3);
    
    	v1[0] = 0;
    
    	TVector<int> v2(v1);
    
    	v1[0] = 1;
    
    	EXPECT_EQ(0, v2[0]);
    }
    
    TEST(TVector, can_get_size) {
    	const int SIZE = 4;
    
    	TVector<int> v(SIZE);
    
    	EXPECT_EQ(SIZE, v.GetSize());
    }
    
    TEST(TVector, can_get_start_index) {
    	const int SIZE = 4, SI = 2;
    
    	TVector<int> v(SIZE, SI);
    
    	EXPECT_EQ(SI, v.GetStartIndex());
    }
    
    TEST(TVector, can_set_and_get_element) {
    	TVector<int> v(4);
    	v[0] = 4;
    
    	EXPECT_EQ(4, v[0]);
    }
    
    TEST(TVector, throws_when_set_element_with_negative_index) {
    	TVector<int> v(2);
    
    	ASSERT_ANY_THROW(v[-1] = 0);
    }
    
    TEST(TVector, throws_when_set_element_with_too_large_index) {
    	TVector<int> v(4, 2);
    
    	ASSERT_ANY_THROW(v[6] = 0);
    }
    
    TEST(TVector, can_assign_vector_to_itself) {
    	TVector<int> v(3);
    
    	ASSERT_NO_THROW(v = v);
    }
    
    TEST(TVector, can_assign_vectors_of_equal_size) {
    	TVector<int> v1(5), v2(5);
    
    	v2 = v1;
    
    	EXPECT_EQ(v1, v2);
    }
    
    TEST(TVector, assign_operator_change_vector_size) {
    	const int SIZE1 = 5, SIZE2 = 3;
    
    	TVector<int> v1(SIZE1), v2(SIZE2);
    
    	v2 = v1;
    
    	EXPECT_EQ(SIZE1, v2.GetSize());
    }
    
    TEST(TVector, can_assign_vectors_of_different_size) {
    	const int SIZE1 = 5, SIZE2 = 3;
    
    	TVector<int> v1(SIZE1), v2(SIZE2);
    
    	v2 = v1;
    
    	EXPECT_EQ(v1, v2);
    }
    
    TEST(TVector, compare_equal_vectors_return_true) {
    	TVector<int> v1(5), v2(v1);
    
    	EXPECT_EQ(1, v1 == v2);
    }
    
    TEST(TVector, compare_vector_with_itself_return_true) {
    	TVector<int> v(5);
    
    	EXPECT_EQ(1, v == v);
    }
    
    TEST(TVector, vectors_with_different_size_are_not_equal) {
    	const int SIZE1 = 5, SIZE2 = 3;
    
    	TVector<int> v1(SIZE1), v2(SIZE2);
    
    	EXPECT_EQ(1, v1 != v2);
    }
    
    TEST(TVector, can_add_scalar_to_vector) {
    	const int SIZE = 5;
    
    	TVector<int> v1(SIZE), v2(SIZE);
    
    	for (int i = 0; i < v1.GetSize(); i++)
    		v1[i] = 1;
    
    	for (int i = 0; i < v2.GetSize(); i++)
    		v2[i] = 5;
    
    	v1 = v1 + 4;
    
    	EXPECT_EQ(v1, v2);
    }
    
    TEST(TVector, can_multiply_scalar_by_vector) {
    	const int SIZE = 5;
    
    	TVector<int> v1(SIZE), v2(SIZE);
    
    	for (int i = 0; i < v1.GetSize(); i++)
    		v1[i] = 1;
    
    	for (int i = 0; i < v2.GetSize(); i++)
    		v2[i] = 5;
    
    	v1 = v1 * 5;
    
    	EXPECT_EQ(v1, v2);
    }
    
    TEST(TVector, can_add_vectors_with_equal_size) {
    	const int SIZE = 5;
    
    	TVector<int> v1(SIZE), v2(SIZE), v3(SIZE);
    
    	for (int i = 0; i < v1.GetSize(); i++)
    		v1[i] = 1;
    
    	for (int i = 0; i < v2.GetSize(); i++)
    		v2[i] = 5;
    
    	for (int i = 0; i < v3.GetSize(); i++)
    		v3[i] = 6;
    
    	EXPECT_EQ(v3, v1 + v2);
    }
    
    TEST(TVector, cant_add_vectors_with_not_equal_size) {
    	const int SIZE1 = 5, SIZE2 = 3;
    
    	TVector<int> v1(SIZE1), v2(SIZE2);
    
    	ASSERT_ANY_THROW(v1 + v2);
    }
    
    TEST(TVector, can_subtract_vectors_with_equal_size) {
    	const int SIZE = 5;
    
    	TVector<int> v1(SIZE), v2(SIZE), v3(SIZE);
    
    	for (int i = 0; i < v1.GetSize(); i++)
    		v1[i] = 1;
    
    	for (int i = 0; i < v2.GetSize(); i++)
    		v2[i] = 5;
    
    	for (int i = 0; i < v3.GetSize(); i++)
    		v3[i] = 4;
    
    	EXPECT_EQ(v3, v2 - v1);
    }
    
    TEST(TVector, cant_subtract_vectors_with_not_equal_size) {
    	const int SIZE1 = 5, SIZE2 = 3;
    
    	TVector<int> v1(SIZE1), v2(SIZE2);
    
    	ASSERT_ANY_THROW(v1 - v2);
    }
    
    TEST(TVector, can_multiply_vectors_with_equal_size) {
    	const int SIZE = 5;
    
    	TVector<int> v1(SIZE), v2(SIZE);
    
    	for (int i = 0; i < v1.GetSize(); i++)
    		v1[i] = 1;
    
    	for (int i = 0; i < v2.GetSize(); i++)
    		v2[i] = 5;
    
    	EXPECT_EQ(25, v1 * v2);
    }
    
    TEST(TVector, cant_multiply_vectors_with_not_equal_size) {
    	const int SIZE1 = 5, SIZE2 = 3;
    
    	TVector<int> v1(SIZE1), v2(SIZE2);
    
    	ASSERT_ANY_THROW(v1 * v2);
    }



### ����� ��� ������ TMatrix:

    #include "utmatrix.h"
    
    #include <gtest.h>
    
    TEST(TMatrix, can_create_matrix_with_positive_length) {
    	ASSERT_NO_THROW(TMatrix<int> m(5));
    }
    
    TEST(TMatrix, cant_create_too_large_matrix) {
    	ASSERT_ANY_THROW(TMatrix<int> m(MAX_MATRIX_SIZE + 1));
    }
    
    TEST(TMatrix, throws_when_create_matrix_with_negative_length) {
    	ASSERT_ANY_THROW(TMatrix<int> m(-5));
    }
    
    TEST(TMatrix, can_create_copied_matrix) {
    	TMatrix<int> m(5);
    
    	ASSERT_NO_THROW(TMatrix<int> m1(m));
    }
    
    TEST(TMatrix, copied_matrix_is_equal_to_source_one) {
    	TMatrix<int> m1(5), m2(m1);
    
    	EXPECT_EQ(m1, m2);
    }
    
    TEST(TMatrix, copied_matrix_has_its_own_memory) {
    	TMatrix<int> m1(3);
    
    	m1[0][0] = 0;
    
    	TMatrix<int> m2(m1);
    
    	m1[0][0] = 1;
    
    	EXPECT_EQ(0, m2[0][0]);
    }
    
    TEST(TMatrix, can_get_size) {
    	const int SIZE = 5;
    
    	TMatrix<int> m(SIZE);
    
    	EXPECT_EQ(SIZE, m.GetSize());
    }
    
    TEST(TMatrix, can_set_and_get_element) {
    	TMatrix<int> m(2);
    	m[0][0] = 4;
    
    	EXPECT_EQ(4, m[0][0]);
    }
    
    TEST(TMatrix, throws_when_set_element_with_negative_index) {
    	TMatrix<int> m(2);
    
    	ASSERT_ANY_THROW(m[-1][0] = 0);
    }
    
    TEST(TMatrix, throws_when_set_element_with_too_large_index) {
    	TMatrix<int> m(4);
    
    	ASSERT_ANY_THROW(m[4][0] = 0);
    }
    
    TEST(TMatrix, can_assign_matrix_to_itself) {
    	TMatrix<int> m(3);
    
    	ASSERT_NO_THROW(m = m);
    }
    
    TEST(TMatrix, can_assign_matrices_of_equal_size) {
    	const int SIZE = 5;
    
    	TMatrix<int> m1(SIZE), m2(SIZE);
    
    	m2 = m1;
    
    	EXPECT_EQ(m1, m2);
    }
    
    TEST(TMatrix, assign_operator_change_matrix_size) {
    	const int SIZE1 = 5, SIZE2 = 3;
    
    	TMatrix<int> m1(SIZE1), m2(SIZE2);
    
    	m2 = m1;
    
    	EXPECT_EQ(5, m2.GetSize());
    }
    
    TEST(TMatrix, can_assign_matrices_of_different_size) {
    	const int SIZE1 = 5, SIZE2 = 3;
    
    	TMatrix<int> m1(SIZE1), m2(SIZE2);
    
    	m2 = m1;
    
    	EXPECT_EQ(m1, m2);
    }
    
    TEST(TMatrix, compare_equal_matrices_return_true) {
    	const int SIZE = 5;
    
    	TMatrix<int> m1(SIZE), m2(SIZE);
    
    	for (int i = 0; i < SIZE; i++)
    		for (int j = i; j < SIZE; j++)
    			m1[i][j] = 1;
    
    	m2 = m1;
    
    	EXPECT_EQ(1, m1 == m2);
    }
    
    TEST(TMatrix, compare_matrix_with_itself_return_true) {
    	const int SIZE = 5;
    
    	TMatrix<int> m(SIZE);
    
    	EXPECT_EQ(1, m == m);
    }
    
    TEST(TMatrix, matrices_with_different_size_are_not_equal) {
    	const int SIZE1 = 5, SIZE2 = 3;
    
    	TMatrix<int> m1(SIZE1), m2(SIZE2);
    
    	EXPECT_EQ(1, m1 != m2);
    }
    
    TEST(TMatrix, can_add_matrices_with_equal_size) {
    	const int SIZE = 5;
    
    	TMatrix<int> m1(SIZE), m2(SIZE), m3(SIZE);
    
    	for (int i = 0; i < SIZE; i++)
    		for (int j = i; j < SIZE; j++)
    			m1[i][j] = 1;
    
    	for (int i = 0; i < SIZE; i++)
    		for (int j = i; j < SIZE; j++)
    			m2[i][j] = 2;
    
    	for (int i = 0; i < SIZE; i++)
    		for (int j = i; j < SIZE; j++)
    			m3[i][j] = 3;
    
    	EXPECT_EQ(m3, m1 + m2);
    }
    
    TEST(TMatrix, cant_add_matrices_with_not_equal_size) {
    	const int SIZE1 = 5, SIZE2 = 3;
    
    	TMatrix<int> m1(SIZE1), m2(SIZE2);
    
    	ASSERT_ANY_THROW(m1 + m2);
    }
    
    TEST(TMatrix, can_subtract_matrices_with_equal_size) {
    	const int SIZE = 5;
    
    	TMatrix<int> m1(SIZE), m2(SIZE), m3(SIZE);
    
    	for (int i = 0; i < SIZE; i++)
    		for (int j = i; j < SIZE; j++)
    			m1[i][j] = 1;
    
    	for (int i = 0; i < SIZE; i++)
    		for (int j = i; j < SIZE; j++)
    			m2[i][j] = 2;
    
    	for (int i = 0; i < SIZE; i++)
    		for (int j = i; j < SIZE; j++)
    			m3[i][j] = 3;
    
    	EXPECT_EQ(m1, m3 - m2);
    }
    
    TEST(TMatrix, cant_subtract_matrixes_with_not_equal_size) {
    	const int SIZE1 = 5, SIZE2 = 3;
    
    	TMatrix<int> m1(SIZE1), m2(SIZE2);
    
    	ASSERT_ANY_THROW(m1 - m2);
    }



### ���������� ������������

��� ����� ���� ������� ��������. ��������� ������������:

![](http://savepic.ru/12980694.jpg)

## ���������� ����� samplematrix.cpp

    #include <iostream>
    #include "utmatrix.h"
    //---------------------------------------------------------------------------
    
    void main()
    {
    	const int SIZE = 3;
    
    	TMatrix<int> a(SIZE), b(SIZE), c(SIZE);
    	int i, j;
    
    	setlocale(LC_ALL, "Russian");
    	cout << "������������ �������� ��������� ������������� ����������� ������"
    	<< endl;
    	for (i = 0; i < SIZE; i++)
    		for (j = i; j < SIZE; j++ )
    		{
    		a[i][j] =  i + j;
    		b[i][j] =  (i + j) * 2;
    		}
    	c = a + b;
    	cout << "Matrix a = " << endl << a << endl;
    	cout << "Matrix b = " << endl << b << endl;
    	cout << "Matrix c = a + b" << endl << c << endl;
    
    	c = a - b;
    	cout << "Matrix a = " << endl << a << endl;
    	cout << "Matrix b = " << endl << b << endl;
    	cout << "Matrix c = a - b" << endl << c << endl;
    
    	for (i = 0; i < SIZE; i++)
    		for (j = i; j < SIZE; j++)
    			a[i][j] *= 2;
    
    	cout << "Matrix a = " << endl << a << endl;
    	cout << "Matrix b = " << endl << b << endl;
    	if (a == b)
    		cout << "Matrix a = b" << endl << endl;
    	else
    		cout << "Matrix a != b" << endl << endl;
    
    	TMatrix<int> d(SIZE + 1);
    
    	try
    	{
    		cout << "Matrix a size = " << a.GetSize() << endl;
    		cout << "Matrix d size = " << d.GetSize() << endl;
    		cout << "Matrix a + d = " << endl << (c = a + d) << endl;
    	}
    	catch (int a)
    	{
    		if (a == 3)
    			cout << "Can't add matrices with different size!" << endl;
    	}
    }


![](http://savepic.ru/13022697.jpg)

## ������

������������� �������� ������� ��������� �������� ������������ ���� ��� ������ � ����������������� ��������. ������������ ������� TMatrix ����� � ������� ������ TVector ���������� ����������������� ����, ������� �� �������� ������ ������ ��� ������ TMatrix.
���� ���� �������� ������ � ����� ������ ��� ��� ������������. �� �������� ���� ����������� ������������� ��������. ������� ����� ������� ������ ������������ ���������, ������� � ����� ������ �������� ��������� �����������������. ������� ������� �������� ������ �� ������ � ����������� ������.