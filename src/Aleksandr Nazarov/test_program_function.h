#pragma once
#include <string>
#include <iostream>
#include "utmatrix.h"
#include "gtest.h"
#include <map>

using namespace std;

void help();
void create(string *arg, map<string, TMatrix<int>> &matrix);
void del(string *arg, map<string, TMatrix<int>> &matrix);
void info(string *arg, map<string, TMatrix<int>> matrix);
void add(string *arg, map<string, TMatrix<int>> &matrix);
void subtract(string *arg, map<string, TMatrix<int>> &matrix);
void push(string *arg, map<string, TMatrix<int>> &matrix);
void equal(string *arg, map<string, TMatrix<int>> matrix);
bool legal_name(string str);
int str_to_int(string str);
bool can_str_to_int(string str);
bool command_list(string command);
void assigned(string *arg, map<string, TMatrix<int>> &matrix);

void help()
{
	cout << "��������: " << endl;
	cout << "'create NAME X' - ������� ������� � ������ NAME ������� X" << endl;
	cout << "'delete NAME' - ������� ������� � ������ NAME" << endl;
	cout << "'assigned X1 X2' - ���������� ������ ������� X2 � ������� X1" << endl;
	cout << "'info' - ������ ��������� ������" << endl;
	cout << "'info X1' - ���������� � �������" << endl;
	cout << "'add X1 X2 X3' - ������� �������/��������� X1 � ��������/���������� X2 � ��������� � ������� X3" << endl;
	cout << "'add X1 X2' - ������� �������/��������� X1 � ��������/���������� X2 � ������� �� ����� ���������" << endl;
	cout << "'subtract X1 X2 X3' - ������� �������/��������� X1 � ��������/���������� X2 � ��������� � ������� X3" << endl;
	cout << "'subtract X1 X2' - ������� �������/��������� X1 � ��������/���������� X2 � ������� �� ����� ���������" << endl;
	cout << "'push NAME x y z' - ��������� ������(x,y) ������� � ������ NAME �������� z" << endl;
	cout << "'equal X1 X2' - �������� ������� X1 � �������� X2 � ������� ��������� �� �����" << endl;
	cout << "'exit' - ����� �� ���������" << endl;
}
void create(string *arg, map<string, TMatrix<int>> &matrix)
{
	if (arg[0] == "" || arg[1] == "" || arg[3] != "")
		throw "�������� ���������� ����������";
	if (legal_name(arg[0]))
	{
		map<string, TMatrix<int>>::iterator it = matrix.find(arg[0]);
		if (it == matrix.end())
			matrix.insert(make_pair(arg[0], TMatrix<int>(str_to_int(arg[1]))));
		else
			throw "��� ������� ��� ����������";
	}
	else
		throw (arg[0] + " - �������� �������� �������");
}
void del(string *arg, map<string, TMatrix<int>> &matrix)
{
	if (arg[0] == "" || arg[1] != "")
		throw "�������� ���������� ����������";
	if (matrix.empty())
		throw "������� " + arg[0] + " �� ����������";
	map<string, TMatrix<int>>::iterator it1;
	it1 = matrix.find(arg[0]);
	if (it1 != matrix.end())
	{
		map<string, TMatrix<int>> buffer;
		map<string, TMatrix<int>>::iterator it2 = matrix.begin();
		for (; it2 != matrix.end(); ++it2)
		{
			if (it1 != it2)
				buffer.insert(*it2);
		}
		matrix = buffer;
	}
	else
		throw ("������� " + arg[0] + " �� ����������");
}
void info(string *arg, map<string, TMatrix<int>> matrix)
{
	if (matrix.empty()) throw "������ ���";
	if (arg[1] != "") throw "�������� ���������� ����������";
	if (arg[0] == "")
	{
		map<string, TMatrix<int>>::iterator it = matrix.begin();
		cout << "=======================" << endl;
		for (; it != matrix.end(); ++it)
			cout << it->first << endl;
		cout << "=======================" << endl;
	}
	else
	{
		map<string, TMatrix<int>>::iterator it;
		it = matrix.find(arg[0]);
		if (it != matrix.end())
		{
			cout << "=======================" << endl;
			cout << it->second << endl;
			cout << "=======================" << endl;
		}
		else
			throw ("������� " + arg[0] + " �� ����������");
	}
}
void add(string *arg, map<string, TMatrix<int>> &matrix)
{
	if (arg[1] == "" || arg[3] != "") throw "�������� ���������� ����������";
	if (arg[2] == "")
	{
		if (can_str_to_int(arg[0]) && can_str_to_int(arg[1]))
			cout << (str_to_int(arg[0]) + str_to_int(arg[1])) << endl;
		if (!((legal_name(arg[0]) || legal_name(arg[1])) && matrix.size())) throw ((legal_name(arg[0])) ? ("������� " + arg[0] + " �� ����������") : ("������� " + arg[1] + " �� ����������"));
		if (can_str_to_int(arg[0]) && legal_name(arg[1]))
		{
			int num = str_to_int(arg[0]);
			map<string, TMatrix<int>>::iterator it = matrix.find(arg[1]);
			if (it != matrix.end())
			{
				cout << "=======================" << endl;
				for (int i = 0; i < it->second.GetSize(); i++)
					cout << (it->second[i] + num) << endl;
				cout << "=======================" << endl;
			}
			else throw ("������� " + arg[1] + " �� ����������");
		}
		if (can_str_to_int(arg[1]) && legal_name(arg[0]))
		{
			int num = str_to_int(arg[1]);
			map<string, TMatrix<int>>::iterator it = matrix.find(arg[0]);
			if (it != matrix.end())
			{
				cout << "=======================" << endl;
				for (int i = 0; i < it->second.GetSize(); i++)
					cout << (it->second[i] + num) << endl;
				cout << "=======================" << endl;
			}
			else throw ("������� " + arg[1] + " �� ����������");
		}
		if (legal_name(arg[0]) && legal_name(arg[1]))
		{
			map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[0]);
			map<string, TMatrix<int>>::iterator it2 = matrix.find(arg[1]);
			if (it1 != matrix.end() && it2 != matrix.end())
			{
				cout << "=======================" << endl;
				cout << (it1->second + it2->second) << endl;
				cout << "=======================" << endl;
			}
			else throw ((it1 == matrix.end()) ? ("������� " + arg[0] + " �� ����������") : ("������� " + arg[1] + " �� ����������"));
		}
	}
	else
	{
		map<string, TMatrix<int>>::iterator it = matrix.find(arg[2]);
		if (can_str_to_int(arg[0]) && can_str_to_int(arg[1]))
			throw "����� ���� ����� ������ �������������� � �������";
		if (matrix.size())
		{
			if (it == matrix.end())
				throw ("������� " + arg[2] + " �� ����������");
		}
		else
		{
			throw ("������� " + arg[2] + " �� ����������");
		}
		if (can_str_to_int(arg[0]) && legal_name(arg[1]))
		{
			int num = str_to_int(arg[0]);
			map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[1]);
			if (it1 != matrix.end())
			{
				for (int i = 0; i < it1->second.GetSize(); i++)
					it->second[i] = (it1->second[i] + num);
			}
			else throw ("������� " + arg[1] + " �� ����������");
		}
		if (can_str_to_int(arg[1]) && legal_name(arg[0]))
		{
			int num = str_to_int(arg[1]);
			map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[0]);
			if (it1 != matrix.end())
			{
				for (int i = 0; i < it1->second.GetSize(); i++)
					it->second[i] = (it1->second[i] + num);
			}
			else throw ("������� " + arg[0] + " �� ����������");
		}
		if (legal_name(arg[0]) && legal_name(arg[1]))
		{
			map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[0]);
			map<string, TMatrix<int>>::iterator it2 = matrix.find(arg[1]);
			if (it1 != matrix.end() && it2 != matrix.end())
				it->second = (it1->second + it2->second);
			else throw ((it1 == matrix.end()) ? ("������� " + arg[0] + " �� ����������") : ("������� " + arg[1] + " �� ����������"));
		}
	}
}
void subtract(string *arg, map<string, TMatrix<int>> &matrix)
{
	if (arg[1] == "" || arg[3] != "") throw "�������� ���������� ����������";
	if (arg[2] == "")
	{
		if (can_str_to_int(arg[0]) && can_str_to_int(arg[1]))
			cout << (str_to_int(arg[0]) + str_to_int(arg[1])) << endl;
		if (!((legal_name(arg[0]) || legal_name(arg[1])) && matrix.size())) throw ((legal_name(arg[0])) ? ("������� " + arg[0] + " �� ����������") : ("������� " + arg[1] + " �� ����������"));
		if (can_str_to_int(arg[0]) && legal_name(arg[1]))
		{
			int num = str_to_int(arg[0]);
			map<string, TMatrix<int>>::iterator it = matrix.find(arg[1]);
			if (it != matrix.end())
			{
				cout << "=======================" << endl;
				for (int i = 0; i < it->second.GetSize(); i++)
					cout << (it->second[i] - num) << endl;
				cout << "=======================" << endl;
			}
			else throw ("������� " + arg[1] + " �� ����������");
		}
		if (can_str_to_int(arg[1]) && legal_name(arg[0]))
		{
			int num = str_to_int(arg[1]);
			map<string, TMatrix<int>>::iterator it = matrix.find(arg[0]);
			if (it != matrix.end())
			{
				cout << "=======================" << endl;
				for (int i = 0; i < it->second.GetSize(); i++)
					cout << (it->second[i] - num) << endl;
				cout << "=======================" << endl;
			}
			else throw ("������� " + arg[1] + " �� ����������");
		}
		if (legal_name(arg[0]) && legal_name(arg[1]))
		{
			map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[0]);
			map<string, TMatrix<int>>::iterator it2 = matrix.find(arg[1]);
			if (it1 != matrix.end() && it2 != matrix.end())
			{
				cout << "=======================" << endl;
				cout << (it1->second - it2->second) << endl;
				cout << "=======================" << endl;
			}
			else throw ((it1 == matrix.end()) ? ("������� " + arg[0] + " �� ����������") : ("������� " + arg[1] + " �� ����������"));
		}
	}
	else
	{
		map<string, TMatrix<int>>::iterator it = matrix.find(arg[2]);
		if (can_str_to_int(arg[0]) && can_str_to_int(arg[1]))
			throw "����� ���� ����� ������ �������������� � �������";
		if (matrix.size())
		{
			if (it == matrix.end())
				throw ("������� " + arg[2] + " �� ����������");
		}
		else
		{
			throw ("������� " + arg[2] + " �� ����������");
		}
		if (can_str_to_int(arg[0]) && legal_name(arg[1]))
		{
			int num = str_to_int(arg[0]);
			map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[1]);
			if (it1 != matrix.end())
			{
				for (int i = 0; i < it1->second.GetSize(); i++)
					it->second[i] = (it1->second[i] - num);
			}
			else throw ("������� " + arg[1] + " �� ����������");
		}
		if (can_str_to_int(arg[1]) && legal_name(arg[0]))
		{
			int num = str_to_int(arg[1]);
			map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[0]);
			if (it1 != matrix.end())
			{
				for (int i = 0; i < it1->second.GetSize(); i++)
					it->second[i] = (it1->second[i] - num);
			}
			else throw ("������� " + arg[0] + " �� ����������");
		}
		if (legal_name(arg[0]) && legal_name(arg[1]))
		{
			map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[0]);
			map<string, TMatrix<int>>::iterator it2 = matrix.find(arg[1]);
			if (it1 != matrix.end() && it2 != matrix.end())
				it->second = (it1->second - it2->second);
			else throw ((it1 == matrix.end()) ? ("������� " + arg[0] + " �� ����������") : ("������� " + arg[1] + " �� ����������"));
		}
	}
}
void push(string *arg, map<string, TMatrix<int>> &matrix)
{
	if (arg[3] == "" || arg[4] != "")
		throw "�������� ���������� ����������";
	if (legal_name(arg[0]) && can_str_to_int(arg[1]) && can_str_to_int(arg[2]) && can_str_to_int(arg[3]))
	{
		map<string, TMatrix<int>>::iterator it = matrix.find(arg[0]);
		if (it != matrix.end())
			it->second[str_to_int(arg[1])][str_to_int(arg[2])] = str_to_int(arg[3]);
		else
			throw ("������� " + arg[0] + "�� ����������");
	}
	else throw "������������ ���������";
}
void equal(string *arg, map<string, TMatrix<int>> matrix)
{
	if (arg[1] == "" || arg[2] != "")
		throw "�������� ���������� ����������";
	if (legal_name(arg[0]) && legal_name(arg[1]))
	{
		map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[0]);
		map<string, TMatrix<int>>::iterator it2 = matrix.find(arg[1]);
		if (it1 != matrix.end() && it2 != matrix.end())
		{
			cout << (it1->second == it2->second) << endl;
		}
		else
			throw (it1 == matrix.end()) ? ("������� " + arg[0] + "�� ����������") : ("������� " + arg[1] + "�� ����������");
	}
	else throw "������������ ���������";
}
void assigned(string *arg, map<string, TMatrix<int>> &matrix)
{
	if (arg[1] == "" || arg[2] != "")
		throw "�������� ���������� ����������";
	if (legal_name(arg[0]) && legal_name(arg[1]))
	{
		map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[0]);
		map<string, TMatrix<int>>::iterator it2 = matrix.find(arg[1]);
		if (it1 != matrix.end() && it2 != matrix.end())
			it1->second = it2->second;
		else
			throw (it1 == matrix.end()) ? ("������� " + arg[0] + "�� ����������") : ("������� " + arg[1] + "�� ����������");
	}
	else throw "������������ ���������";
}
bool legal_name(string str)
{
	if ((str.at(0) >= 'A' && str.at(0) <= 'Z') || (str.at(0) >= 'a' && str.at(0) <= 'z'))
	{
		for (int i = 1; i < str.size(); i++)
		{
			if (!((str.at(i) >= 'A' && str.at(i) <= 'Z') || (str.at(i) >= 'a' && str.at(i) <= 'z') || (str.at(i) >= '0' && str.at(i) <= '9') || str.at(i) == '_'))
			{
				return false;
			}
		}
		return true;
	}
	else
		return false;
}
int str_to_int(string str)
{
	int result = 0;
	if (str.at(0) != '-')
	{
		for (int i = str.size() - 1, f = 0; i >= 0; i--, f++)
		{
			if (str.at(i) >= '0' && str.at(i) <= '9')
				result = result + pow(10, f)*(str.at(i) - '0');
			else
				throw ("�������� " + str + " ������ ���� ������");
		}
	}
	else
	{
		for (int i = str.size() - 1, f = 0; i > 0; i--, f++)
		{
			if (str.at(i) >= '0' && str.at(i) <= '9')
				result = result - pow(10, f)*(str.at(i) - '0');
			else
				throw ("�������� " + str + " ������ ���� ������");
		}
	}
	return result;
}
bool can_str_to_int(string str)
{
		for (int i = ((str.at(0) != '-')?0:1); i < str.size(); i++)
			if (str.at(i) < '0' || str.at(i) > '9')
				return false;
	return true;
}
bool command_list(string command)
{
	string commands[10] = { "create", "delete", "info", "add", "subtract", "push", "equal", "exit", "help", "assigned" };
	for (int i = 0; i < 10; i++)
		if (command == commands[i])
			return false;
	return true;
}
